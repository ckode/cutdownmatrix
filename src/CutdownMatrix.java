/*  SonzoSoft Cutdown Matrix: Add in nouns and verbs to mix and match new cutdowns!
    Copyright (C) 2010  David C. Brown - SonzoSoft

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import javax.swing.*;
import javax.swing.table.*;


public class CutdownMatrix extends JApplet
{
	DataEntry DataEntryPanel;
	public static JTabbedPane cmTabbedPane;
	public static CutdownOutput CutdownPanel;
	
	private static final long serialVersionUID = 1L;

	public void init()
	{
		try {
			SwingUtilities.invokeAndWait(new Runnable() {
						public void run() {
							StartGUI();
						}
			});
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  	}

  	private void StartGUI()
  	{
  	  DataEntryPanel = new DataEntry();
  	  CutdownPanel = new CutdownOutput();
	  cmTabbedPane = new JTabbedPane();
	  cmTabbedPane.addTab("Enter Cutdowns", DataEntryPanel);
	  cmTabbedPane.addTab("View Cutdowns", CutdownPanel);

	  add(cmTabbedPane);
	  setSize(490, 250);
  	}
}
  
 
@SuppressWarnings("serial")
class DataEntry extends JPanel
{
	static JTextField N1;
	static JTextField N2;
	static JTextField N3;
	static JTextField N4;
	static JTextField N5;
	
	static JTextField V1;
	static JTextField V2;
	static JTextField V3;
	static JTextField V4;
	static JTextField V5;
	

	public DataEntry()
	{
		setLayout(new GridLayout(7, 2, 10, 10));
		
		JLabel NounLabel = new JLabel("Enter 5 Nouns", JLabel.CENTER);
		JLabel VerbLabel = new JLabel("Enter 5 Verbs", JLabel.CENTER);
	
        N1 = new JTextField(15);
        N2 = new JTextField(15);
        N3 = new JTextField(15);
        N4 = new JTextField(15);
        N5 = new JTextField(15);
        
        V1 = new JTextField(15);      
        V2 = new JTextField(15);
        V3 = new JTextField(15);
        V4 = new JTextField(15);
        V5 = new JTextField(15);
        
        JButton Submit = new SubmitButton();
        JButton Reset  = new ResetButton();

        add(NounLabel);
        add(VerbLabel);
        
        add(N1);
        add(V1);
        
        add(N2);
        add(V2);
        
        add(N3);
        add(V3);
        
        add(N4);
        add(V4);
        
        add(N5);
        add(V5);
        
        add(Submit);
        add(Reset);
		
	}
	
	
	// Create reset button
	private static class ResetButton extends JButton
		implements ActionListener {
		String text = "Reset";
		public ResetButton()
		{
			super();
			setText(text);
			addActionListener(this);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			N1.setText(null);
			N2.setText(null);
			N3.setText(null);
			N4.setText(null);
			N5.setText(null);
			V1.setText(null);
			V2.setText(null);
			V3.setText(null);
			V4.setText(null);
			V5.setText(null);
		}
	}
	
	// Create reset button
	private static class SubmitButton extends JButton
		implements ActionListener {
		String text = "Submit";
		public SubmitButton()
		{
			super();
			setText(text);
			addActionListener(this);
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			Object[][] data = {
					{ N1.getText() + " " + V1.getText(), V1.getText() + " " + N1.getText() },
					{ N1.getText() + " " + V2.getText(), V2.getText() + " " + N1.getText() },
					{ N1.getText() + " " + V3.getText(), V3.getText() + " " + N1.getText() },
					{ N1.getText() + " " + V4.getText(), V4.getText() + " " + N1.getText() },
					{ N1.getText() + " " + V5.getText(), V5.getText() + " " + N1.getText() },
					{ N2.getText() + " " + V1.getText(), V1.getText() + " " + N2.getText() },
					{ N2.getText() + " " + V2.getText(), V2.getText() + " " + N2.getText() },
					{ N2.getText() + " " + V3.getText(), V3.getText() + " " + N2.getText() },
					{ N2.getText() + " " + V4.getText(), V4.getText() + " " + N2.getText() },
					{ N2.getText() + " " + V5.getText(), V5.getText() + " " + N2.getText() },
					{ N3.getText() + " " + V1.getText(), V1.getText() + " " + N3.getText() },
					{ N3.getText() + " " + V2.getText(), V2.getText() + " " + N3.getText() },
					{ N3.getText() + " " + V3.getText(), V3.getText() + " " + N3.getText() },
					{ N3.getText() + " " + V4.getText(), V4.getText() + " " + N3.getText() },
					{ N3.getText() + " " + V5.getText(), V5.getText() + " " + N3.getText() },
					{ N4.getText() + " " + V1.getText(), V1.getText() + " " + N4.getText() },
					{ N4.getText() + " " + V2.getText(), V2.getText() + " " + N4.getText() },
					{ N4.getText() + " " + V3.getText(), V3.getText() + " " + N4.getText() },
					{ N4.getText() + " " + V4.getText(), V4.getText() + " " + N4.getText() },
					{ N4.getText() + " " + V5.getText(), V5.getText() + " " + N4.getText() },
					{ N5.getText() + " " + V1.getText(), V1.getText() + " " + N5.getText() },
					{ N5.getText() + " " + V2.getText(), V2.getText() + " " + N5.getText() },
					{ N5.getText() + " " + V3.getText(), V3.getText() + " " + N5.getText() },
					{ N5.getText() + " " + V4.getText(), V4.getText() + " " + N5.getText() },
					{ N5.getText() + " " + V5.getText(), V5.getText() + " " + N5.getText() },							
			};
			CutdownMatrix.CutdownPanel.tableModel.updateTable(data);
			CutdownMatrix.cmTabbedPane.setSelectedIndex(1);
		}
	}
}

@SuppressWarnings("serial")
class CutdownOutput extends JPanel
{
	MatrixTableModel tableModel;
	class MatrixTableModel extends AbstractTableModel 
	{
	    private String[] columnNames = { "Cutdowns", "Cutdowns" };
	    private Object[][] data = {
                	{ " ", " " },
				    { " ", " " },
				    { " ", " " },
			   	    { " ", " " },
				    { " ", " " },
				    { " ", " " },
				    { " ", " " },
				    { " ", " " },
				    { " ", " " },
				    { " ", " " },
				    { " ", " " },
				    { " ", " " },
				    { " ", " " },
				    { " ", " " },
				    { " ", " " },
				    { " ", " " },
				    { " ", " " },
				    { " ", " " },
				    { " ", " " },
				    { " ", " " }
             };

	    
		public void updateTable( Object[][] newData )
	    {
	    	this.data = newData;
	    }
		@Override
        public int getColumnCount() {
            return columnNames.length;
        }

        public int getRowCount() {
            return data.length;
        }

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			return data[rowIndex][columnIndex];
		}	
	}
	
	public CutdownOutput()
	{
	   tableModel = new MatrixTableModel();
       JTable table = new JTable(tableModel) 
       {
			public boolean isCellEditable(int rowIndex, int colIndex) 
			{
    	         return false;   //Disallow the editing of any cell
    	    }
       };

       table.setPreferredScrollableViewportSize(new Dimension(460, 200));     
       table.setFillsViewportHeight(true);
       
       JScrollPane scrollPane = new JScrollPane(table);
       // Shrink of columns width to 35 characters
       TableColumn column = null;      
 
       for (int i = 0; i < 2; i++) {

           column = table.getColumnModel().getColumn(i);
           column.setPreferredWidth(35);
       }	
       add(scrollPane, BorderLayout.CENTER);
	}
} 
